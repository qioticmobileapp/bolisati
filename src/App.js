
import React, {Component} from 'react';
import {createStore, applyMiddleware} from "redux";
import {View,Text,StatusBar,Keyboard} from 'react-native';
import {Drawer, Root} from "native-base";
import {Provider} from 'react-redux';
import ReduxThunk from "redux-thunk";
import rootReducer from './reducers';
import SideBar from "./components/sideBar";
import Header from './components/header';
import Router from './Router';
// import store from './Store'
class App extends React.Component{
  closeDrawer = () => {
    this.drawer._root.close();
  };

  openDrawer = () => {
    
    this.drawer._root.open();
    setTimeout(() => Keyboard.dismiss());
  };
  render() {
    const store = createStore(rootReducer, {}, applyMiddleware(ReduxThunk));

    return (
      <Provider store={store}>
          <Root>
          <Drawer
            type="overlay"
            side="right"
            ref={ref => {
              this.drawer = ref;
            }}
            content={
              <SideBar
                navigator={this._navigator}
                closeDrawer={this.closeDrawer}
              />
            }
            onClose={this.closeDrawer}
            onOpen={this.openDrawer}
            tapToClose={true}
            openDrawerOffset={0.2}
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            styles={drawerStyles}
          >
           <Header
                       openDrawer={this.openDrawer}
                       closeDrawer={this.closeDrawer}
           />
            <StatusBar backgroundColor="#1e2131" barStyle="light-content" />
            <Router />

          </Drawer>
        </Root>
     
   
      </Provider>
    );
  }
}
const drawerStyles = {
  drawer: {shadowOpacity: 0, elevation: 0},
  main: {shadowOpacity: 0, elevation: 0}
};
export default App