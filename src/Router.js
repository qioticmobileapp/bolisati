import React, {Component} from "react";
import {Scene, Router} from "react-native-router-flux";
import FirstPage from './components/firstPage';
import SplashScreen from "./components/splashScreen";
import {isLoggIn} from "./actions/authAction";
import SignUp from './components/signUp';
import Login from './components/login';
import Home from './components/home';
import DrivingLicense from "./components/drivingLicense";
import CarInformation from "./components/carInformation";
import InsuranceCompanies from './components/insuranceCompanies';
import {connect} from "react-redux";
import {AsyncStorage} from 'react-native';
import DamageStep from "./components/damageStep";

// const RouterController = props => {
  class RouterController extends Component{
    state = {
      isUserLogin: false
    }
    componentDidMount() {
       AsyncStorage.getItem('user', (err, result) => {
        if (result != null) {
          this.setState({ isUserLogin: true });
        }
        else{
          this.setState({ isUserLogin: false })
        }
       
      });
    }
    authenticate = () => {
      this.state.isUserLogin ? true : false
    }
    render(){
      return (
        <Router
        navigationBarStyle={{backgroundColor: "#fff"}}
      >
        <Scene key="root" hideNavBar="hideNavBar" >
          <Scene key="Bolisati">
          <Scene 
              key="Launch" 
              component="Launch"
              initial
              on={this.authenticate}
              success="Home"
              failure="FirstPage"
            />
            <Scene
              key="firstpage"
              component={FirstPage}
              title=""
              hideNavBar
              initial={!this.state.isUserLogin}
            />
           
            <Scene
              key="splashscreen"
              component={SplashScreen}
              title=""
              hideNavBar
              
            />
             <Scene
              key="login"
              component={Login}
              title=""
              hideNavBar
            />
            <Scene
              key="signup"
              component={SignUp}
              title=""
              hideNavBar
            />
            <Scene
              key="home"
              component={Home}
              title=""
              hideNavBar
              initial={this.state.isUserLogin}
            />
            <Scene
              key="drivinglicense"
              component={DrivingLicense}
              title=""
              hideNavBar 
            />
            <Scene
              key="carinformation"
              component={CarInformation}
              title=""
              hideNavBar 
            />
            <Scene
              key="insurancecompanies"
              component={InsuranceCompanies}
              title=""
              hideNavBar 
            />
             <Scene
              key="damagestep"
              component={DamageStep}
              title=""
              hideNavBar 
            />
          </Scene>
        </Scene>
      </Router>
    );
}
  };
  
  export default RouterController;

  