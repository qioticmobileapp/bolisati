import {
    GET_USER_TEXT,
    START_AUTH_LOADING,
    SIGININ_MSG,
    SIGINUP_MSG,
    RESET_MSG,
    SHOW_RECOVER_PASSWORD_MODAL,
    RECOVER_PASS_MSG,
    CET_RECOVER_PASSWORD_EMAIL_TEXT,
    IS_USER_LOGGEDIN,
    SHOW_MOBILE_CODE_MODAL
    
  } from './types';
  import { Keyboard } from "react-native";
  import axios from 'axios';
  import { Actions } from 'react-native-router-flux';
  import { AsyncStorage } from 'react-native';
import client from '../constants';
  const validate_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  // const base_URL = 'https://bolisati.qiotic.info/app';
  //START GETTING INPUTS TEXT ACTION
export const getUserText = ({prop, value}) => {
    const obj = {type: GET_USER_TEXT,payload: {prop,value}};
    return obj;
  };
  //END GETTING INPUTS TEXT ACTION

  //START  LOGIN USER ACTION
export const loginUser = (phone_number, password) => {


    return (dispatch) => {
      console.log("password",password)
      if (phone_number.number ==''||password == '') {
        dispatch({
          type: SIGININ_MSG,
          payload: {
            isError: true,
            isSuccess: false,
            msg: "Please make sure you've entered your phone number and password"
          }
        });
  
      } 
   
        else {
          Keyboard.dismiss();
          dispatch({
            type: START_AUTH_LOADING,
            payload: {
              signin_loading: true,
              signup_loading: false,
              recover_loading: false
            }
          });
          client.post(`processlogin`, {
            customers_telephone	: phone_number.number,
            password: password
          }).then(function(response) {
            console.log("response12345",response)
            dispatch({
              type: START_AUTH_LOADING,
              payload: {
                signin_loading: false,
                signup_loading: false,
                recover_loading: false
              }
            });
       
            if (response.data.status=="200")
         {
          dispatch({
            type: IS_USER_LOGGEDIN,
            payload: {
              isLoggedIn: true,
              data: response.data.data
            }
          });
            AsyncStorage.setItem("user",JSON.stringify(response.data.data));
            console.log("hoooooooon")
            Actions.home();
         }
         else if (response.data.success!="200"){
           console.log("here response.data.success",response.data.message)
          dispatch({
            type: SIGININ_MSG,
            payload: {
              isError: true,
              isSuccess: false,
              msg: response.data.message
            }
          });
         }
          }).catch(function(error) {
            console.log("error",error)
            const res = JSON.parse(error.request._response);
            dispatch({
              type: SIGININ_MSG,
              payload: {
                isError: true,
                isSuccess: false,
                msg: "error in something"
              }
            });
            dispatch({
              type: START_AUTH_LOADING,
              payload: {
                signin_loading: false,
                signup_loading: false,
                recover_loading: false
              }
            });
          
          });
        }
      }
  
    // }
  };
  //END LOGIN USER ACTION

  //START CHECK USER IF LOGGED IN
export const isLoggIn = () => {
  return dispatch => {
    AsyncStorage.getItem("user").then(
      res => {
        dispatch({
          type: IS_USER_LOGGEDIN,
          payload:
            res != null
              ? {
                  isLoggedIn: true,
                  data: JSON.parse(res)
                }
              : {
                  isLoggedIn: false,
                  data: null
                }
        });
      },
      error => {
        dispatch({
          type: IS_USER_LOGGEDIN,
          payload: {
            isLoggedIn: false,
            data: null
          }
        });
      }
    );
  };
};
//END CHECK USER IF LOGGED IN

  //START RESETTING ERROR AND SUCCESS MESSAGES ACTION
export const resetAuthMessage = () => {
    return ({type: RESET_MSG})
  }
  //END RESETTING ERROR AND SUCCESS MESSAGES ACTION

  //START SHOW/HIDE MODAL
export const showRecoverPasswordModal = value => {
  return {type: SHOW_RECOVER_PASSWORD_MODAL, payload: value};
};
//END SHOW/HIDE MODAL

  //START SHOW/HIDE MODAL
export const showMobileCodeModal = value => {
  return {type: SHOW_MOBILE_CODE_MODAL, payload: value};
};
//END SHOW/HIDE MODAL
  

export const recoverPassword = (phone_number) => {
  return (dispatch) => {
    dispatch({type: RESET_MSG});
    if (phone_number.number == '') {
      dispatch({
        type: RECOVER_PASS_MSG,
        payload: {
          isError: true,
          isSuccess: false,
          msg: "Number is required"
        }
      });

    }

    else {
      dispatch({
        type: START_AUTH_LOADING,
        payload: {
          signin_loading: false,
          signup_loading: false,
          recover_loading: true
        }
      });
      client.post(`processforgotpassword?customers_telephone=${phone_number.number}`).then(function(response) {
        console.log("responsee",response)
        dispatch({
          type: START_AUTH_LOADING,
          payload: {
            signin_loading: false,
            signup_loading: false,
            recover_loading: false
          }
        });
        dispatch({type: SHOW_RECOVER_PASSWORD_MODAL, payload: false});

        dispatch({
          type: RECOVER_PASS_MSG,
          payload: {
            isError: false,
            isSuccess: true,
            msg: response.data.message
          }
        });
      }).catch(function(error) {
        console.log("error",error)
        const res = JSON.parse(error.request._response);
        dispatch({
          type: START_AUTH_LOADING,
          payload: {
            signin_loading: false,
            signup_loading: false,
            recover_loading: false
          }
        });
        dispatch({type: RECOVER_PASS_MSG, payload: response.data.message});

      });
    }
  }
};
//END ACTION CREATOR FOR RECOVER PASSWORD INSIDE SIGN IN


//START SIGN UP USER ACTION
export const signupUser = (first_name, last_name, password,phone_number,confirm_password) => {
  console.log("client",axios)
  console.log("first_name",first_name)
  console.log("last name",last_name)
  console.log("phone number",phone_number.number)
  console.log("password",password)
  return (dispatch) => {
    if (first_name == '' || last_name == '' || password == ''||phone_number.number==''||confirm_password=='') {
      dispatch({
        type: SIGINUP_MSG,
        payload: {
          isError: true,
          isSuccess: false,
          msg: "Please make sure to fill out all fields!"
        }
      });
   

    } else {
    
        if (password != confirm_password) {
          dispatch({
            type: SIGINUP_MSG,
            payload: {
              isError: true,
              isSuccess: false,
              msg: "New password and confirm password does not match!"
            }
          });
      } 
      // else {
      //   Keyboard.dismiss();
      //   dispatch({
      //     type: START_AUTH_LOADING,
      //     payload: {
      //       signin_loading: false,
      //       signup_loading: true,
      //       recover_loading: false
      //     }
      //   });
        else{
          Keyboard.dismiss();

        client.post(`processregistration`, {
          customers_firstname: first_name,
          customers_lastname: last_name,
          customers_telephone:phone_number.number,
          password: password
      

        }).then(function(response) {
          console.log("response",response)
          dispatch({
            type: START_AUTH_LOADING,
            payload: {
              signin_loading: false,
              signup_loading: false,
              recover_loading: false
            }
          });
          dispatch({
            type: SIGINUP_MSG,
            payload: {
              isError: false,
              isSuccess: true,
              msg: "You have successfully created an account, please check your phone for verification"
            }
          });
          // dispatch({type: SHOW_MOBILE_CODE_MODAL, payload: true});
Actions.login()
        }).catch(function(error) {
          console.log("error11111111",error)
          const res = JSON.parse(error.request._response);
          console.log("res",res)
          dispatch({
            type: SIGINUP_MSG,
            payload: {
              isError: true,
              isSuccess: false,
              msg: res.message
            }
          });
          dispatch({
            type: START_AUTH_LOADING,
            payload: {
              signin_loading: false,
              signup_loading: false,
              recover_loading: false
            }
          });

        });
      }
    }
  }
};
//END SIGN UP USER ACTION

//START SIGN OUT USER
export const signOut = () => {
  return dispatch => {
    AsyncStorage.removeItem("user");
    dispatch({
      type: IS_USER_LOGGEDIN,
      payload: {isLoggedIn: false, data: null}
    });

  };
};
//END SIGN OUT USER

