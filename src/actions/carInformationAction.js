import axios from 'axios';
import {GET_CAR_INFORMATION_TEXTS,RESET_CAR_INFORMATION_MESSAGE,GET_CARS,GET_CARS_MODAL,CAR_INFORMATIOB_MESSAGE} from './types';
import client from '../constants';
import { AsyncStorage } from 'react-native';
import {Actions} from "react-native-router-flux";

//  const base_URL = 'https://bolisati.qiotic.info/app';

//START GETTING INPUTS TEXT ACTION
export const getCarInformationTexts = ({prop, value}) => {
    return dispatch => {
      dispatch({type: GET_CAR_INFORMATION_TEXTS, payload: {prop, value}});
    }
  };
  //END GETTING INPUTS TEXT ACTION

  //START SHOW/HIDE MODAL
export const resetCarInformationMessage = value => {
    return {type:RESET_CAR_INFORMATION_MESSAGE};
  };

  
  //END SHOW/HIDE MODAL

  //START FETCHING CARS
export const getCars = () => {
return (dispatch) => {
  client.post(`getcartype`).then((response) => {

    const res = response.data.data;
    dispatch({type: GET_CARS, payload: res})
  }).catch((error) => {
      console.log("error")
    dispatch({type: GET_CARS, payload: []})
  })
}
};
//END FETCHING CARS


  //START FETCHING CARS
  export const getCarsModal = (car_id) => {
    return (dispatch) => {
      client.post(`getcarmodel?car_id=${car_id}`).then((response) => {
    
        const res = response.data.data;
        dispatch({type: GET_CARS_MODAL, payload: res})
      }).catch((error) => {
          console.log("error")
        dispatch({type: GET_CARS_MODAL, payload: []})
      })
    }
    };
    //END FETCHING CARS

    export const goToInsuranceCompanies = (full_name, id_number, insurance_type,car_type,vehicle_number,car_model,manufacturing_year,driver,fuel_type,car_salary,start_date,end_date) => {
      console.log("client",axios)
      console.log("full_name",full_name)
      console.log("id_number",id_number)
      console.log("insurance_type",insurance_type)
      console.log("car_type",car_type)
      console.log("vehicle_number",vehicle_number)
      console.log("car_model",car_model)
      console.log("manufacturing_year",manufacturing_year)
      console.log("driver",driver)
      console.log("fuel_type",fuel_type)
      console.log("car_salary",car_salary)
      console.log("start_date",start_date)
      console.log("end_date",end_date)




      return (dispatch) => {
        if (full_name == '' || id_number == '' || insurance_type == ''||car_type==''||vehicle_number==''||car_model==''||manufacturing_year==''||driver==''||fuel_type==''||car_salary=='',start_date==''||end_date=='') {
          dispatch({
            type: CAR_INFORMATIOB_MESSAGE,
            payload: {
              isError: true,
              isSuccess: false,
              msg: "Please make sure to fill out all fields!"
            }
          });
       
    
        }  
        
            else{
           const data={ full_name: full_name,
            id_number: id_number,
            insurance_type:insurance_type,
            car_type: car_type,
            vehicle_number:vehicle_number,
            car_model:car_model,
            manufacturing_year:manufacturing_year,
            driver:driver,
            fuel_type:fuel_type,
            car_salary:car_salary,
            start_date:start_date,
            end_date:end_date}
            client.post(`orderinsurance`, {
              car_type: 3,
              car_model:5,
              manufacturing_year:2019,
    
            }).then(function(response) {
              console.log("response",response)
              // dispatch({
              //   type: START_AUTH_LOADING,
              //   payload: {
              //     signin_loading: false,
              //     signup_loading: false,
              //     recover_loading: false
              //   }
              // });
              dispatch({
                type: CAR_INFORMATIOB_MESSAGE,
                payload: {
                  isError: false,
                  isSuccess: true,
                  msg: "success"
                }
              });
              Actions.insurancecompanies({
              insuranceCompanies:response.data.data
            
              });
              AsyncStorage.setItem("car_information",JSON.stringify(data));

            }).catch(function(error) {
              console.log("error11111111",error)
              const res = JSON.parse(error.request._response);
              console.log("res",res)
              dispatch({
                type: CAR_INFORMATIOB_MESSAGE,
                payload: {
                  isError: true,
                  isSuccess: false,
                  msg: res.message
                }
              });
              // dispatch({
              //   type: START_AUTH_LOADING,
              //   payload: {
              //     signin_loading: false,
              //     signup_loading: false,
              //     recover_loading: false
              //   }
              // });
    
            });
          }
        }
      
    };
    //END Go  ACTION