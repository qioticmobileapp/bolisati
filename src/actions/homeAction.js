import axios from 'axios';
import {
    GET_CATEGORIES,
    HOME_LOADING
 } from './types';
import client from '../constants';

//  const base_URL = 'https://bolisati.qiotic.info/app';

//START FETCHING CATEGORIES
export const getCategories = () => {
    const language_id=4
  return (dispatch) => {
    dispatch({type: HOME_LOADING, payload: true})
    client.post(`allcategories?language_id=${language_id}`).then((response) => {
  console.log("response",response)
      const res = response.data.data;
      dispatch({type: GET_CATEGORIES, payload: res})
      dispatch({type: HOME_LOADING, payload: false})

    }).catch((error) => {
        console.log("error")
      dispatch({type: GET_CATEGORIES, payload: []})
      dispatch({type: HOME_LOADING, payload: false})

    })
  }
};
//END FETCHING CATEGORIES

