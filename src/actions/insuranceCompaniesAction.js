import {SHOW_INSURANCE_COMPANY_MODAL,GET_ADDONS,GET_TERMS,GET_COMPANIES_TEXT} from './types';
import client from '../constants';

  //START SHOW/HIDE MODAL
  export const showInsuranceCompanyModal = value => {
    return {type: SHOW_INSURANCE_COMPANY_MODAL, payload: value};
  };
  //END SHOW/HIDE MODAL

//START FETCHING ADDONS
export const getAddons = (manufacturers_id) => {
return (dispatch) => {
  client.post(`companyaddons?manufacturers_id=${manufacturers_id}`).then((response) => {
console.log("response",response)
    const res = response.data.data;
    dispatch({type: GET_ADDONS, payload: res})

  }).catch((error) => {
      console.log("error")
    dispatch({type: GET_ADDONS, payload: []})

  })
}
};
//END FETCHING ADDONS

//START FETCHING TERMS
export const getTerms = (manufacturers_id) => {
  return (dispatch) => {
    client.post(`companyinformation?manufacturers_id=${manufacturers_id}`).then((response) => {
  console.log("response of terms",response)
      const res = response.data.data;
      dispatch({type: GET_TERMS, payload: res})
  
    }).catch((error) => {
        console.log("error")
      dispatch({type: GET_TERMS, payload: []})
  
    })
  }
  };
  //END FETCHING TERMS


export const getInsuranceCompaniesText = ({prop, value}) => {
  return dispatch => {
    dispatch({type: GET_COMPANIES_TEXT, payload: {prop, value}});
  }
};
