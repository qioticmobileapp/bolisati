export const SAMPLE_ACTION = 'SAMPLE_ACTION';
export const RECIVE_API_DATA = 'RECIVE_API_DATA'
export const REQUEST_API_DATA = 'REQUEST_API_DATA'
export const SIGININ_MSG = 'signin_msg';
export const SIGINUP_MSG = 'signup_msg';
export const GET_USER_TEXT = 'get_user_text';
export const START_AUTH_LOADING = 'start_auth_loading';
export const IS_USER_LOGGEDIN = 'is_user_loggedin';
export const RESET_MSG = 'reset_msg';
export const RECOVER_PASS_MSG='recover_pass_msg';
export const SHOW_RECOVER_PASSWORD_MODAL='show_recover_password_modal';
export const CET_RECOVER_PASSWORD_EMAIL_TEXT='get_recover_password_email_text';
export const GET_CATEGORIES='get_categories';
export const HOME_LOADING = 'home_loading';
export const SHOW_MOBILE_CODE_MODAL='show_mobile_code_modal';
export const MOBILE_AUTHENTICATION_TEXTS='mobile_authentication_texts';
export const USER_MOBILE_AUTH_INFO='user_mobile_auth_info';
export const MOBILE_AUTHNTICATION_MESSAGE='mobile_authentication_message';
export const MOBILE_AUTHNTICATION_LOADING='mobile_authentication_loading';
export const RESET_MOBILE_AUTHENTICATION_COMPLETELY='reset_mobile_authentication_completely';
export const GET_CAR_INFORMATION_TEXTS='get_car_information_texts';
export const CAR_INFORMATIOB_MESSAGE='car_information_message';
export const RESET_CAR_INFORMATION_MESSAGE='reset_car_information_message';
export const START_LOADING='start_loading';
export const SHOW_INSURANCE_COMPANY_MODAL='show_insurance_company_modal';
export const GET_CARS='get_cars';
export const GET_CARS_MODAL='get_cars_modal';
export const INSURANCE_COMPANIES_LOADING = 'insurance_companies_loading';
export const GET_ADDONS='get_addons';
export const GET_TERMS='get_terms';
export const GET_COMPANIES_TEXT='get_companies_text';

