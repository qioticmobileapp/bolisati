import {React} from 'react';
import { StyleSheet, Platform } from 'react-native';
module.exports  = StyleSheet.create({

uploadButton:{
    backgroundColor:"#ffffff",justifyContent:'center'
},
licenseImage:{
    height:172.8,width:278.3
},
continueButton:{
    backgroundColor:"#003580",width:161, marginRight:10,justifyContent:'center'
},
continueText:{
    color:"#fcfcfc",fontSize:16,fontFamily:'Tajawal-Bold'
},
uploadLicenseText:{
    color:"#171717",fontSize:16,fontFamily:'Tajawal-Bold'
},
skipButton:{
    backgroundColor:"#ffffff",width:161,justifyContent:'center'
},
skipText:{
    color:"#2a2b2b",fontSize:16,fontFamily:'Tajawal-Bold'
}
  })