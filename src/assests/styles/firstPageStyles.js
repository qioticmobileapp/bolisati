import { StyleSheet } from 'react-native';
module.exports  = StyleSheet.create({
cardItem:{
    backgroundColor:"transparent",flex:0.77
},
signInButton:{
    backgroundColor:'#edc68c',justifyContent:'center'
},
signInText:{
    color:"#003580",fontSize:16,fontFamily:'Tajawal-Bold'
},
orText:{
    color:"#171717",marginTop:10,fontFamily:'Tajawal-Regular'
},
signUpButton:{
    backgroundColor:'#003580',justifyContent:'center',marginTop:10
}
  })