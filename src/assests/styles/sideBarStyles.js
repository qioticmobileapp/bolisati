import { StyleSheet } from 'react-native';
module.exports  = StyleSheet.create({
profileNameText:{
    color:"#fff",fontSize:24
},
homeCardItem:{
    backgroundColor:'transparent',marginTop:5
},
otherCardItemStyle:{
    backgroundColor:'transparent',marginTop:20
},
otherTextsStyle:{
    color:"#fff",fontSize:18,fontFamily:'Tajawal-Bold'
},
profileImageStyle:{
    width: 64.5, height: 64.5, borderRadius: 64.5/2
},
listStyle:{
    marginLeft:100
}
  })