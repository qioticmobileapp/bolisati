import React, { Component } from 'react';
import {ImageBackground,Dimensions,ScrollView,View,Image} from 'react-native';
import {CardItem,Body,Button,Text, Icon,Form,Item,Input,Picker,Right,Left,Card} from 'native-base';
import {transparentBackground,transparentBorder,inputStyle,centerStyle,buttonStyle,buttonText,pickerStyle,datePickerStyle} from '../theme';
import * as carInformationAction from '../actions/carInformationAction';
import DropdownAlert from 'react-native-dropdownalert';
import { connect } from 'react-redux';
import DatePicker from 'react-native-datepicker';
import {Actions} from "react-native-router-flux";

const dimensions=Dimensions.get('window');
class CarInformation extends Component{

  componentWillMount() {
    this.props.getCars();
  
    
  }

  componentDidUpdate (){
    const { car_information_msg} = this.props;
     if (car_information_msg != null) {
       setTimeout(()=> this.props.resetCarInformationMessage(),300);
     }
  }
  onValueChange(value) {
    this.props.getCarInformationTexts({prop: "car_type",value})
    this.props.getCarsModal(value)

  }
    //START DROPDOWN MESSAGES
  onError = (error) => {
    if (error) {
    console.log("error",error)
    this.dropdown.alertWithType('error', 'Error', error);
  }
}

  onSuccess = success => {
    if (success) {
    this.dropdown.alertWithType('success', 'Success', success);
  }
}
 
//END DROPDOWN MESSAGES
     //START SHOW ALERT FUNC
     showAlert = () => {
      const {car_information_msg} = this.props;
      if (car_information_msg != null) {
        if (car_information_msg.isError) {
          this.onError(car_information_msg.msg);
        } else if (car_information_msg.isSuccess) {
          this.onSuccess(car_information_msg.msg);
        } else {
          return;
        }
      }
    };
    //END SHOW ALERT FUNC
    goToInsuranceCompanies=()=>{
      const {full_name, id_number, insurance_type,car_type,vehicle_number,car_model,manufacturing_year,driver,fuel_type,car_salary,start_date,end_date} = this.props;
      const end_date1='2020-02-11'
      this.props.goToInsuranceCompanies(full_name, id_number, insurance_type,car_type,vehicle_number,car_model,manufacturing_year,driver,fuel_type,car_salary,start_date,end_date1);
    }

    render(){
      console.log("this.props in car information",this.props)
      const {full_name,insurance_type,car_type,vehicle_number,car_model,manufacturing_year,driver,fuel_type,car_salary,start_date,end_date,id_number,cars,cars_model}=this.props



        return(

            <ImageBackground source={require('../assests/images/splash–1.png')} style={{width:dimensions.width}}>
                      <ScrollView ref={(ref)=> {this._scrollView = ref}}>

              <Card style={{backgroundColor:'transparent',borderColor:'transparent'}}>
               <CardItem style={transparentBackground}>
                 <Item style={transparentBorder}> 
                   <Input
                     color="#fff"
                     value ={full_name}
                     placeholder ="الإسم الرباعي"
                     placeholderTextColor="#9B9B9B"
                     style={inputStyle}
                     onChangeText={value =>this.props.getCarInformationTexts({prop:"full_name",value})}
                   />
                 </Item>
               </CardItem>
               <CardItem style={transparentBackground}>
                 <Item style={transparentBorder}> 
                   <Input
                     color="#fff"
                     value ={id_number}
                     placeholder ="الرقم الوطني"
                     placeholderTextColor="#9B9B9B"
                     style={inputStyle}
                     onChangeText={value =>this.props.getCarInformationTexts({prop:"id_number",value})}
                   />
                 </Item>
               </CardItem>
     
                  <CardItem style={transparentBackground}>
                  <Item style={transparentBorder}>
             
                    <Picker
                      mode="dropdown"
                      iosHeader="فئة التأمين"
                      placeholder="فئة التأمين"
                      iosIcon={<Icon name="arrow-down" />}
                      placeholderStyle={{ color: "#9B9B9B" }}
                      style={pickerStyle}
                      selectedValue={insurance_type}
                      onValueChange={value =>
                        this.props.getCarInformationTexts({
                          prop: "insurance_type",
                          value
                        })
                      }
                     >
                     <Picker.Item label="تكميلي" value="تكميلي" />
                     <Picker.Item label="شامل" value="شامل" />
                    </Picker>
                </Item>
               </CardItem>
               <CardItem style={transparentBackground}>
                  <Item style={transparentBorder}>
             
                    <Picker
                      mode="dropdown"
                      iosHeader="نوع السيارة"
                      placeholder="نوع السيارة"
                      iosIcon={<Icon name="arrow-down" />}
                      placeholderStyle={{ color: "#9B9B9B" }}
                      style={pickerStyle}
                      selectedValue={this.props.car_type}
                      onValueChange={value =>
                        // this.props.getCarInformationTexts({
                        //   prop: "car_type",
                        //   value
                        // })
                        this.onValueChange(value)
                      }
                     >
             
                        {cars.map((item, index) => {
                      return (
                        <Picker.Item
                          key={item.car_id}
                          label={item.car_model}
                          value={item.car_id}
                        />
                      );
                    })}
                    </Picker>
                </Item>
               </CardItem>
               <CardItem style={transparentBackground}>
                  <Item style={transparentBorder}>
             
                    <Picker
                      mode="dropdown"
                      iosHeader="فئة المركبة"
                      placeholder="فئة المركبة"
                      iosIcon={<Icon name="arrow-down" />}
                      placeholderStyle={{ color: "#9B9B9B" }}
                      style={pickerStyle}
                      selectedValue={car_model}
                      onValueChange={value =>
                        this.props.getCarInformationTexts({
                          prop: "car_model",
                          value
                        })
                      }
                     >
                          {cars_model.map((item, index) => {
                      return (
                        <Picker.Item
                          key={item.model_id}
                          label={item.model_name}
                          value={item.model_id}
                        />
                      );
                    })}
                    </Picker>
                </Item>
               </CardItem>
               <CardItem style={transparentBackground}>
                 <Item style={transparentBorder}> 
                   <Input
                     color="#fff"
                     value ={vehicle_number}
                     placeholder ="رقم المركبة"
                     placeholderTextColor="#9B9B9B"
                     style={inputStyle}
                     onChangeText={value =>this.props.getCarInformationTexts({prop: "vehicle_number",value})}
                    />
                 </Item>
               </CardItem> 
   
               <CardItem style={transparentBackground}>
                  <Item style={transparentBorder}>
             
                    <Picker
                      mode="dropdown"
                      iosHeader="سنة الصنع"
                      placeholder="سنة الصنع"
                      iosIcon={<Icon name="arrow-down" />}
                      placeholderStyle={{ color: "#9B9B9B" }}
                      style={pickerStyle}
                      selectedValue={manufacturing_year}
                      onValueChange={value =>
                        this.props.getCarInformationTexts({
                          prop: "manufacturing_year",
                          value
                        })
                      }
                     >
                     <Picker.Item label="2009" value="2009" />
                     <Picker.Item label="2010" value="2010" />
                     <Picker.Item label="2011" value="2011" />
                     <Picker.Item label="2012" value="2012" />
                     <Picker.Item label="2013" value="2013" />
                    </Picker>
                </Item>
               </CardItem>
               <CardItem style={transparentBackground}>
                  <Item style={transparentBorder}>
             
                    <Picker
                      mode="dropdown"
                      iosHeader="ناقل الحركة"
                      placeholder="ناقل الحركة"
                      iosIcon={<Icon name="arrow-down" />}
                      placeholderStyle={{ color: "#9B9B9B" }}
                      style={pickerStyle}
                      selectedValue={driver}
                      onValueChange={value =>
                        this.props.getCarInformationTexts({
                          prop: "driver",
                          value
                        })
                      }
                     >
                     <Picker.Item label="اتوماتيك" value="اتوماتيك" />
                     <Picker.Item label="عادي" value="عادي" />
                    </Picker>
                </Item>
               </CardItem>
               <CardItem style={transparentBackground}>
                  <Item style={transparentBorder}>
             
                    <Picker
                      mode="dropdown"
                      iosHeader="نوع الوقود"
                      placeholder="نوع الوقود"
                      iosIcon={<Icon name="arrow-down" />}
                      placeholderStyle={{ color: "#9B9B9B" }}
                      style={pickerStyle}
                      selectedValue={fuel_type}
                      onValueChange={value =>
                        this.props.getCarInformationTexts({
                          prop: "fuel_type",
                          value
                        })
                      }
                     >
                     <Picker.Item label="بنزين" value="بنزين" />
                    </Picker>
                </Item>
               </CardItem>
               <CardItem style={transparentBackground}>
                              <Item style={transparentBorder}> 
                   <Input
                     color="#fff"
                     value ={car_salary}
                     placeholder ="قيمة السيارة"
                     placeholderTextColor="#9B9B9B"
                     style={inputStyle}
                     onChangeText={value =>this.props.getCarInformationTexts({prop: "car_salary",value})}
                    />
                 </Item>
             
               </CardItem>
               <CardItem style={transparentBackground}>
                 <Item style={transparentBorder}>
                 <DatePicker
                 style={datePickerStyle}
                 date={ start_date}
                 mode="date"
                 placeholder="تاريخ البدء"
                 format="YYYY-MM-DD"
                 minDate={new Date()}
                 maxDate="2029-12-31"
                 confirmBtnText="Confirm"
                 cancelBtnText="Cancel"
                 customStyles={{
                 dateIcon: {
                 position: 'absolute',
                 left: 0,
                 top: 4,
                 marginLeft: 0
                 },
                 dateInput: {
                 marginLeft: 36
                 },
                 btnTextConfirm: {
                 height: 20
                 },
                 btnTextCancel: {
                 height: 20
                 }
     }}
     onDateChange={(value) => {this.props.getCarInformationTexts({prop:'start_date',value})}}
    />
                 </Item>

               </CardItem>

               <CardItem style={transparentBackground}>
               
                <Body style={centerStyle}>
                  {/* <Button style={buttonStyle} block onPress={() => Actions.insurancecompanies()}> */}
                  <Button style={buttonStyle}  block onPress={this.goToInsuranceCompanies}>
                    <Icon name='md-arrow-back' style={{color:'#fff'}}/> 
                    <Text style={buttonText}>متابعة</Text>
        
                  </Button>
                </Body>
            </CardItem>

            </Card>
           
            {/* <View style={{height:50,backgroundColor:"trasparent"}}></View> */}

            </ScrollView>
            <Text>{this.showAlert()}</Text>

<DropdownAlert ref={ref => (this.dropdown = ref)} />
            </ImageBackground>

            

        )
    }
}
// export default CarInformation;
const mapStateToProps = state => {
  const { full_name,insurance_type,car_type,vehicle_number,car_model,manufacturing_year,driver,fuel_type,car_salary,start_date,end_date,information_loading,id_number,cars,cars_model,car_information_msg} = state.carInformarionReducer;
  return {full_name,insurance_type,car_type,vehicle_number,car_model,manufacturing_year,driver,fuel_type,car_salary,start_date,end_date,information_loading,id_number,cars,cars_model,car_information_msg};
}
// END MAP STATE TO PROPS


export default connect(mapStateToProps,carInformationAction)(CarInformation);