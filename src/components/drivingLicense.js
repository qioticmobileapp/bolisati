import React, { Component } from 'react';
import {ImageBackground,Dimensions,Image} from 'react-native';
import {CardItem,Body,Button,Text,Right,Left, Icon, Content} from 'native-base';
import {uploadButton,licenseImage,continueText,uploadLicenseText,skipButton,skipText,continueButton} from '../assests/styles/drivingLicenseStyles';
import {transparentBackground,centerStyle} from '../theme';
import {Actions} from "react-native-router-flux";

const dimensions=Dimensions.get('window');
class DrivingLicense extends Component{
    render(){
  
        return(
            <ImageBackground source={require('../assests/images/splash–1.png')} style={{width:dimensions.width,height:dimensions.height}}>
            <Content style={transparentBackground}>
               <CardItem style={[transparentBackground,{marginTop:30}]}>
                 <Body style={centerStyle}>
                 <Image
                   source={require('../assests/images/Artboard.png')} style={licenseImage}
                 />
                 </Body>
               </CardItem>
               <CardItem style={transparentBackground}>
                 <Body style={centerStyle}>
                   <Button style={uploadButton} block>
                     <Text style={uploadLicenseText}>تحميل رخصة السيارة</Text>
                   </Button>
                 </Body>
               </CardItem>
               <CardItem style={transparentBackground}>
                 <Button style={continueButton}>
                   <Icon name='md-arrow-back' style={{color:'#fff'}}/> 
                   <Text style={continueText}>متابعة</Text>
                 </Button>
                 <Button style={skipButton}onPress={() => Actions.carinformation()}>
                   <Text style={skipText}>تخطي</Text>
                 </Button>
               </CardItem>
               </Content>
           </ImageBackground>
        )
    }
}
export default DrivingLicense;