import React,{Component} from 'react';
import {ImageBackground,Dimensions,Text} from 'react-native';
import {Body,CardItem,Button} from 'native-base';
import {Actions} from "react-native-router-flux";
import{centerStyle,buttonText} from '../theme'
import {cardItem,signInButton,signInText,orText,signUpButton} from '../assests/styles/firstPageStyles';

const dimensions=Dimensions.get('window');

class FirstPage extends Component{
    render(){
      return(
<ImageBackground source={require('../assests/images/splash–1.png')} style={{width:dimensions.width,height:dimensions.height}}>
<CardItem style={cardItem}>
         <Body style={centerStyle}>
          <Button style={signInButton}block onPress={() => Actions.login()}>
            <Text style={signInText}>تسجيل الدخول</Text>
          </Button>
          <Text style={orText}>أو</Text>
          <Button style={signUpButton}block onPress={() => Actions.signup()}>
            <Text style={[buttonText,{fontFamily:'Tajawal-Bold'}]}> إنشاء حساب</Text>
          </Button>
         </Body>
        </CardItem>
</ImageBackground>
      )

      
    }
}
export default FirstPage;