import React, { Component } from 'react';
import {Dimensions,Image} from 'react-native';
import { connect } from 'react-redux';
import { Container, Header, Left, Body, Right, Title, Button, Text,Icon, CardItem } from 'native-base';
import { Actions} from "react-native-router-flux";
import { isLoggIn } from "../actions/authAction";
import {centerStyle,headerStyle,menuIcon} from '../theme';

// import Icon from "react-native-vector-icons/FontAwesome";
const dimensions=Dimensions.get('window');
class HeaderCustom extends Component {
  componentDidMount(){
    this.props.isLoggIn()
  }
    render(){
      const {user}=this.props;
      console.log("user in header",user)
        return(
            
            <Header style={headerStyle}>
            <Left>
            {/* {user.isLoggedIn? */}
            <Icon name='md-arrow-back' style={{color:'#fff'}} onPress={() =>Actions.pop() }/>
            {/* // :null} */}
            </Left>
            <Body style={centerStyle}>
            <Image
           source={require('../assests/images/BOLISATILOGO-02.png')}
          />
            </Body>
            <Right>
          {user.isLoggedIn ?
              <Button transparent onPress={this.props.openDrawer} >
                <Icon name='menu' style={menuIcon} />
              </Button>
              :null}
        </Right>
   
          </Header>
        )
    }
}
// export default HeaderCustom;
const mapStateToProps = state =>
{
  const { user } = state.authReducer;


  return { user};
}
export default connect(mapStateToProps,{isLoggIn})(HeaderCustom);