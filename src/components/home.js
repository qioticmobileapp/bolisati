import React,{Component} from 'react';
import {ImageBackground,Dimensions,Text,ScrollView,View,TouchableOpacity,Image} from 'react-native';
import {CardItem,Card,Left,Right,Body,Container,Content} from 'native-base';
import{Bold} from './common/Bold';
import {imagesStyle,imagesCradItem,categoriesText} from '../assests/styles/homeStyles';
import {centerStyle,servicesText,sevicesCardItemStyle,transparentBackGround} from '../theme';
import {Actions} from "react-native-router-flux";
import {Spinner} from "./common/Spinner";
import * as homeAction from '../actions/homeAction';
import { connect } from 'react-redux';
const IMAGE_BASE_URL='https://bolisati1.qiotic.info/'
const dimensions=Dimensions.get('window');
class Home extends Component{
    componentWillMount() {
        this.props.getCategories();
        
      }
render(){
    const {categories,home_loading}=this.props;
    console.log("categories",categories);

    return(
        // <ScrollView ref={(ref)=> {this._scrollView = ref}}>
        <ImageBackground source={require('../assests/images/splash–1.png')} style={{width:dimensions.width,height:'100%'}}>

<Container style={{backgroundColor:"transparent"}}>
<Content style={{backgroundColor:"transparent"}}>
{!home_loading?
(categories.length>0?
<View style={[imagesCradItem,{width:dimensions.width}]}>
{categories.map((category, index) => {
 return (
    
  <View style={[imagesCradItem,{width:dimensions.width}]} key={category.id}>
<TouchableOpacity onPress={() => Actions.drivinglicense()}>


<ImageBackground source={{uri: `${IMAGE_BASE_URL}${category.image}`}} style={imagesStyle}>

<CardItem style={{backgroundColor:'transparent'}}>
<Right style={{marginTop:20}}>

<Image source={{uri: `${IMAGE_BASE_URL}${category.icon}`}} style={{height:50,width:50}}/>
<Text style={{color:"#ffffff",fontSize:14,fontFamily:'Tajawal-Bold'}}>
     {category.name}
    </Text>
<Text style={{color:"#ffffff",fontSize:10,fontFamily:'Tajawal-Bold'}}>
{category.descriptionar}
</Text>
</Right>


</CardItem>


</ImageBackground> 
 </TouchableOpacity>
     </View>
   )

})}
 </View>
 :null)
 :
//  <ImageBackground source={require('../assests/images/splash–1.png')} style={{width:dimensions.width,height:'100%'}}>

 <CardItem style={{backgroundColor:'tranparent'}} >
 <Body>
   <View style={{  alignItems:'center',alignSelf:'center',justifyContent:'center',marginTop:250}}>
     <Spinner Size="large" color="#003580" />
   </View>
 </Body>
</CardItem>
// </ImageBackground>
} 
 </Content>
</Container>
</ImageBackground>


    )
}
}
// export default Home

// START MAP STATE TO PROPS
const mapStateToProps = state => {
    const { categories,home_loading} = state.homeReducer;
    return { categories,home_loading};
  }
  // END MAP STATE TO PROPS
  
  
  export default connect(mapStateToProps,homeAction)(Home);