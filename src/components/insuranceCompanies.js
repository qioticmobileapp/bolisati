import React,{Component} from 'react';
import {ImageBackground,Dimensions,Text,ScrollView,View,Image,TouchableWithoutFeedback,Modal} from 'react-native';
import {CardItem,Body,Right,Left,ListItem,CheckBox,Button, Content} from 'native-base';
import { connect } from 'react-redux';

const dimensions=Dimensions.get('window');
const IMAGE_BASE_URL='https://bolisati1.qiotic.info/'

import * as insuranceCompaniesAction from '../actions/insuranceCompaniesAction';

import {centerStyle,servicesText,sevicesCardItemStyle,transparentBackground,buttonStyle,buttonText} from '../theme';
import { Actions } from 'react-native-router-flux';


class InsuranceCompanies extends Component{
  state ={isAddonChecked:false};
  // constructor(props){
  //   super(props);
  //   this.state ={
  //     ss:false    }

  // }

  countPrice=(value)=>{
    let{addons}=this.props;
    console.log("value",value)
    value.isChecked=!value.isChecked
          this.setState({isAddonChecked:value.isChecked});

    // value.isChecked=!value.isChecked
    // console.log("value22",value)
    // this.props.getInsuranceCompaniesText({
    //     prop: value.isChecked,
    //   value: !value.isChecked
    // })
    // console.log("addons",addons)
  }
    goToAnotherPage=()=>{
        const {show_insurance_modal}=this.props;
        this.props.showInsuranceCompanyModal(!show_insurance_modal)
        Actions.damagestep()
    }
    showModal=(id)=>{
      const {addons,show_insurance_modal}=this.props;
      this.props.getAddons(id);
      this.props.getTerms(id);
      this.props.showInsuranceCompanyModal(!show_insurance_modal)

 console.log("addons",addons)
    }
    render(){
        const {show_insurance_modal,insuranceCompanies,terms,addons}=this.props
        var total=0
        for( var i=0;i<addons.length;i++)
        {
        if(addons[i].isChecked){
          total=total+addons[i].price
        }
        }
        console.log("insuranceCompanies",insuranceCompanies)
        console.log("addons2222",addons)
        console.log("terms",terms)
        return(
            <ImageBackground source={require('../assests/images/splash–1.png')} style={{width:dimensions.width,height:'100%'}}>
              {/* <TouchableWithoutFeedback onPress={() => this.props.showInsuranceCompanyModal(!show_insurance_modal)}>    */}
<Content style={{backgroundColor:"transparent"}}>
              <CardItem style={[sevicesCardItemStyle, {width: dimensions.width}]}>
 
                <Right>
                  <Text style={servicesText}>شركات التأمين</Text>
                </Right>
              </CardItem>
              {/* <TouchableWithoutFeedback onPress={() =>Actions.damagestep()}>   

              <CardItem style={{backgroundColor:"#fff",height:129,marginTop:15,width:dimensions.width,direction:'rtl'}}>

               <Right style={{marginLeft:150}}>
                 <Image source={require('../assests/images/insurance.jpeg')} style={{width:238,height:113}}/>
               </Right>
               <Left style={{marginTop:70,alignSelf:"flex-end"}}>
                 <Body>
                 <Text>423.00 JOD</Text>
                 </Body>
               </Left>

              </CardItem>
              </TouchableWithoutFeedback> */}
          {insuranceCompanies.length>0?
          insuranceCompanies.map((company, index) => {
            return (
           <TouchableWithoutFeedback onPress={() => this.showModal(company.manufacturers_id)} key={company.manufacturers_id}>
              <CardItem style={{backgroundColor:"#fff",height:129,marginTop:15,width:dimensions.width,direction:'rtl'}}>
               <Right>
                 <Body>
                 <Image source={{uri: `${IMAGE_BASE_URL}${company.manufacturers_image}`}} style={{width:120,height:120}}/>
                 </Body>
               </Right>
               <Body style={{justifyContent:"center",alignItems:"center"}}>
                 <Text style={{marginLeft:50}}>
                   {company.manufacturers_name}
                 </Text>
               </Body>
               <Left>
                 <Body>
                 <Text>{company.insurance_price}</Text>
                 </Body>
               </Left>
              </CardItem>
              </TouchableWithoutFeedback>
          )})
:null}
              <Modal
            visible={show_insurance_modal}
            animationType={"slide"}
            onRequestClose={() =>
              this.props.showInsuranceCompanyModal(!show_insurance_modal)
            }
            supportedOrientations={[
              "portrait",
              "portrait-upside-down",
              "landscape",
              "landscape-left",
              "landscape-right"
            ]}
            transparent
          >
             <TouchableWithoutFeedback
              onPress={() => this.props.showInsuranceCompanyModal(!show_insurance_modal)}
            >
              <View style={{backgroundColor:'rgba(0,0,0,0.50)',position:'relative',flex:1,justifyContent:'center'}}>
                <View style={{  borderWidth:1,borderRadius:5,borderColor:'#e3e3e3',padding:0,backgroundColor:'#fff',marginLeft:15,marginRight:15}}>
                  <ListItem style={{marginTop:10,alignSelf:"flex-end",borderBottomWidth: 0}}>
                    <Text style={{color:"#171717",fontSize:21}}>
                    الشروط والأحكام
                    </Text>
                  </ListItem>
                  {terms.length>0?
                  <ListItem style={{alignSelf:"flex-end",borderBottomWidth: 0}}>   
                      <Text style={{fontSize:12}}>
                          {terms[0].terms}
                      </Text>
                       
                  </ListItem>
                    :null}

                  <ListItem style={{marginTop:10,alignSelf:"flex-end",borderBottomWidth: 0}}>
                    <Text style={{color:"#171717",fontSize:21}}>
                    الإضافات

                    </Text>
                  </ListItem>
                  {/* <ListItem>
                  <Left>
                   <Text>hhhhhh</Text>
                    </Left>
                    <Right>
                      <CheckBox
                        style={{marginRight:15,borderRadius:50}}
                        checked={this.state.ss}
                        color="#003580"
                        onPress={() => this.setState({ss: !this.state.ss})}
                      />
                    </Right>
                  </ListItem> */}
             {addons.length>0?
              addons.map((addon, index) => {
                addon.isChecked=Boolean(addon.isChecked)
                console.log("addon.isChecked",addon.isChecked)
                return(
            <ListItem style={{borderBottomWidth: 0}} key={index}>
            <Right>
              <Body>
              <CheckBox
                        style={{marginRight:15,borderRadius:50}}
                        checked={addon.isChecked}
                        color="#003580"
                        onPress={() =>this.countPrice(addon)
                      
                        }
                      />
              </Body>
              </Right>
              <Left>
                <Body>
                <Text>
                {addon.addon_name} {addon.price}
                </Text>
                </Body>
              </Left>
            </ListItem>
              )})
            :null}
            <CardItem>
            <Right>
                    <Text>{total} JOD</Text>
                </Right>
                <Body>

                </Body>
                <Left>
                    <Text>المجموع</Text>
                    
                </Left>
            
            </CardItem>
            <ListItem style={{borderBottomWidth: 0}}>
                    <Body>
                    <Button
            onPress={this.goToAnotherPage}
            style={{backgroundColor:"#003580"}}
            block
          
          >
            <Text style={buttonText}>أوافق على الشروط والأحكام</Text>
          </Button>
                    </Body>
                  </ListItem>

                </View>
              </View>
              </TouchableWithoutFeedback>
          </Modal>
          </Content>
            </ImageBackground>

        )
    }

}
// START MAP STATE TO PROPS
const mapStateToProps = state => {
    const { show_insurance_modal,addons,terms} = state.insuranceCompaniesReducer;
    return { show_insurance_modal,addons,terms};
  }
  // END MAP STATE TO PROPS
  
  
  export default connect(mapStateToProps,insuranceCompaniesAction)(InsuranceCompanies);