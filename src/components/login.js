import React,{Component} from 'react';
import  {ImageBackground,Dimensions,Text,StyleSheet,TouchableOpacity,View,Modal,TouchableWithoutFeedback,Keyboard,Image} from 'react-native';
import {Body,CardItem,Item,Button,Input,Content,List,
  ListItem,Icon,Toast} from 'native-base';
import {Spinner} from "./common/Spinner";
import DropdownAlert from 'react-native-dropdownalert';
import PhoneInput from 'react-native-phone-input';
import * as authAction from '../actions/authAction';
import { connect } from 'react-redux';
import {Actions} from "react-native-router-flux";
import {blueBackgroundColor,buttonStyle,buttonText,cardItemWithMargin,inputStyle,transparentBackground,transparentBorder,phoneInputStyle,centerStyle,touchableOpacityText} from '../theme';
const dimensions=Dimensions.get('window');
class Login extends Component{
  constructor(props){
    super(props);
    this.state ={
      hidePassword:true    }

  }
    componentDidUpdate (){
        const { signin_alert_message} = this.props;
         if (signin_alert_message != null) {
           setTimeout(()=> this.props.resetAuthMessage(),250);
         }
      }
        //START DROPDOWN MESSAGES
 onError = (error) => {
    if (error) {
      console.log("error",error)
      this.dropdown.alertWithType('error', 'Error', error);
    }
  }

onSuccess = success => {
      if (success) {
        this.dropdown.alertWithType('success', 'Success', success);
      }
  }
//END DROPDOWN MESSAGES
 //START SHOW ALERT FUNC
 showAlert = () =>
   {
       const { signin_alert_message} = this.props;
        if (signin_alert_message != null) {

          if (signin_alert_message.isError) {
         
            this.onError(signin_alert_message.msg);
            
          }
          else if(signin_alert_message.isSuccess)
          {
            this.onSuccess(signin_alert_message.msg);

          }
          else {
            return ;
          }
        }
     }

 //END SHOW ALERT FUNC

   //START SHOW TOAST
   showToast = () => {
    const {recover_password_msg} = this.props;
    if (recover_password_msg != null) {
      if (recover_password_msg.isSuccess) {
        setTimeout(
          () =>
            Toast.show({
              text: recover_password_msg.msg,
              buttonText: "Okay",
              position: "bottom",
              type: "success"
            }),
          500
        );
      }
    }
  };
  //END SHOW TOAST

//START SIGNIN HANDLER
  signInHandler = ()=>
  {
    const { phone_number,password } = this.props;
        this.props.loginUser(phone_number,password);

  
    
  }
//END SIGNIN HANDLER



//START SHOW SUBMIT BUTTON
  showSubmitButton = ()=>
  {
    const { start_loading_of } = this.props;

      if (start_loading_of.signin_loading) {
        return (
          <Button
            block
            style={blueBackgroundColor}>
            <Spinner size="large" />
          </Button>
        );
      }

    return (
   
    <Button style={{backgroundColor:'#003580',justifyContent:'center',marginTop:10}}block onPress={this.signInHandler}>
{/* <Button style={buttonStyle} block onPress={() => Actions.home()}>  */}
       <Text style={buttonText}> تسجيل الدخول</Text>
     </Button>

    )
  }
  //END SHOW SUBMIT BUTTON

  //START FORGET PASSWORD MODAL
  openForgetPasswordModal = () => {
    const {show_modal} = this.props;

    this.props.showRecoverPasswordModal(!show_modal);
    this.props.resetAuthMessage();
    this.props.getUserText({prop: "recover_password_email", value: ""});
  };
  //END FORGET PASSWORD MODAL

    //START RECOVER PASSWORD BUTTON
    renderRecoverPasswordBtn = () => {
      const {start_loading_of} = this.props;
      if (start_loading_of.recover_loading) {
        return (
          <Button block style={blueBackgroundColor}>
            <Spinner size="large" />
          </Button>
        );
      }
      return (
        <Button
          block
          style={blueBackgroundColor}
          onPress={this.recoverPassword}
        >
          <Text style={{color: "#fff"}}>
          إستعادة كلمة المرور
          </Text>
        </Button>
      );
    };
    //END RECOVER PASSOWRD BUTTON

      //START RECOVER PASSWORD
  recoverPassword = () => {
    const {phone_number} = this.props;
    this.props.recoverPassword(phone_number);
  };
  //END RECOVER PASSWORD

     // START SELECT A PHONE COUNTRY
     selectCountry(country){
      const country_code  = this.phone.getCountryCode();
      this.props.getUserText({prop:'phone_number',value:{
        code:country_code,
        number:''
      }});
     }
  
  // END SELECT A PHONE COUNTRY

    // START CHANGING A PHONE NUMBER FUNC
    changePhoneNumber(number)
    {
  
      const { phone_number } = this.props;
     if (this.phone.getISOCode()) {
       const country_code  = this.phone.getCountryCode();
   
       this.props.getUserText({prop:'phone_number',value:{
         code:country_code,
         number:phone_number.number
       }});
     }
     else {
       this.props.getUserText({prop:'phone_number',value:{
         code:'',
         number:phone_number.number
       }});
     }
 const num = number;
     this.props.getUserText({prop:'phone_number',value:{
       code:phone_number.code,
       number:num
     }});
 
   };
    // END CHANGING A PHONE NUMBER FUNC
    managePasswordVisibility=()=>{
      this.setState({hidePassword: !this.state.hidePassword});
    }
    render(){
        const { phone_number, password,show_modal,recover_password_email,recover_password_msg,signin_alert_message} = this.props;

return(
  <ImageBackground source={require('../assests/images/splash–1.png')} style={styles.ImageBackgroundStyle}>
  
  <Content
    ref={ref => {
      this._Content = ref;
    }}
  >

          <CardItem style={[cardItemWithMargin,{justifyContent:'center'}]}>
              <Item style={transparentBorder}>
                {/* <Input
                  color="#000"
                  onChangeText={value =>this.props.getUserText({prop:'email',value})}
                  value ={email}
                  placeholder ="البريد الالكتروني"
                  style={{  borderRadius:5,marginBottom:5,backgroundColor:"#fff",textAlign:"right"}}
                /> */}

<PhoneInput  ref={(ref) => { this.phone = ref; }}
block
                    initialCountry='jo'
                    style={phoneInputStyle}
                    value ={phone_number.number}
                    flagStyle={{marginLeft:10,height:30,width:40,flex:0.2}}
                    textStyle={{fontSize:18}}
                    onSelectCountry ={(country) => this.selectCountry(country)}
                    onChangePhoneNumber= {(number)=> this.changePhoneNumber(number)}
                    isValidNumber
                    
                  /> 

              </Item>
            </CardItem>

            <CardItem style={transparentBackground}>
              <Item style={transparentBorder}> 
                <Input
                  color="#fff"
                  onChangeText={value =>this.props.getUserText({prop:'password',value})}
                  value ={password}
                  placeholder ="كلمة المرور"
                  secureTextEntry={this.state.hidePassword}
                  style={inputStyle}
                />
                       <TouchableOpacity activeOpacity={0.8} style={{position: 'absolute',height: 40,width: 35,padding: 5}} onPress={this.managePasswordVisibility}>
                  <Image source={(this.state.hidePassword) ? require('../assests/images/hideeye.png') : require('../assests/images/eye.png')} style={{resizeMode:'contain',height: '70%',width: '70%'}} />
                </TouchableOpacity>
              </Item>
         

            </CardItem>
            <View style={{marginRight:30}}>
<TouchableOpacity onPress={this.openForgetPasswordModal}>
                <Text style={{fontSize:10,textAlign:"right",color:"#171717",fontFamily:'Tajawal-Bold'}}>هل نسيت كلمة المرور؟</Text>
                </TouchableOpacity>
            </View>
            <CardItem style={transparentBackground}>
         <Body style={centerStyle}>
         { this.showSubmitButton() }
         </Body>
            </CardItem>
            <CardItem style={transparentBackground}>
                <Body style={centerStyle}>
                    <Text style={{fontSize:10,fontFamily:'Tajawal-Medium'}}>أو</Text>
                </Body>
            </CardItem>
            <CardItem style={transparentBackground}>
         <Body style={centerStyle}>
         <TouchableOpacity onPress={() => Actions.signup()}>
         
            <Text style={touchableOpacityText}> إنشاء حساب</Text>
         
          </TouchableOpacity> 
           </Body>
        </CardItem>
        <Modal
            visible={show_modal}
            animationType={"slide"}
            onRequestClose={() =>
              this.props.showRecoverPasswordModal(!show_modal)
            }
            supportedOrientations={[
              "portrait",
              "portrait-upside-down",
              "landscape",
              "landscape-left",
              "landscape-right"
            ]}
            transparent
          >
            <TouchableWithoutFeedback
              onPress={() => this.props.showRecoverPasswordModal(!show_modal)}
            >
              <View style={{backgroundColor:'rgba(0,0,0,0.50)',position:'relative',flex:1,justifyContent:'center'}}>
                <View style={{  borderWidth:1,borderRadius:5,borderColor:'#e3e3e3',padding:0,backgroundColor:'#F7F9F9',marginLeft:15,marginRight:15}}>
                  <ListItem style={{marginTop:10,alignSelf:"center",borderBottomWidth: 0}}>
                    <Text style={{color:"#25579A",fontSize:22}}>
                      إستعادة كلمة المرور
                    </Text>
                  </ListItem>
                  <ListItem style={{borderBottomWidth: 0}}>
                    <Body>
                      {/* <Item style={{borderColor:"#fafafa",borderBottomWidth:0}}>
                        <Input
                          color="#000"
                          onChangeText={value =>
                            this.props.getUserText({
                              prop: "recover_password_email",
                              value
                            })
                          }
                          value={recover_password_email}
                          placeholder="البريد الالكتروني"
                          style={inputStyle}
                        />
                      </Item> */}
              <Item style={transparentBorder}>
           

<PhoneInput  ref={(ref) => { this.phone = ref; }}
                    style={{borderRadius:5,marginBottom:5,backgroundColor:"#fff",textAlign:"right",width:300,height:45}}
                    initialCountry='jo'
                    value ={phone_number.number}
                    flagStyle={{marginLeft:10,height:30,width:40,flex:0.2}}
                    textStyle={{fontSize:18}}
                    onSelectCountry ={(country) => this.selectCountry(country)}
                    onChangePhoneNumber= {(number)=> this.changePhoneNumber(number)}
                    isValidNumber
                    
                  /> 

              </Item>
                      {recover_password_msg ? (
                        recover_password_msg.isError ? (
                          <Item error>
                            {/* <Icon name="close-circle" style={{fontSize: 20}} /> */}
                            <Text style={{color: "red", fontSize: 15}}>
                              <Text>{recover_password_msg.msg}</Text>
                            </Text>
                          </Item>
                        ) : null
                      ) : null}
                    </Body>
                  </ListItem>

                  <ListItem style={{borderBottomWidth: 0}}>
                    <Body>{this.renderRecoverPasswordBtn()}</Body>
                  </ListItem>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
       </Content>
       <Text>{this.showToast()}</Text>

       <Text>{this.showAlert()}</Text>
  
  <DropdownAlert replaceEnabled={true}  ref={(ref) => this.dropdown = ref}  />    
        
</ImageBackground>

)
    }
}
const styles = StyleSheet.create({
    ImageBackgroundStyle: {
        width:dimensions.width,
        height:dimensions.height
    }
  });


// START MAP STATE TO PROPS
const mapStateToProps = state => {
    const { password, phone_number, start_loading_of, signin_alert_message,show_modal,recover_password_email,recover_password_msg} = state.authReducer;
    return { password, phone_number, start_loading_of, signin_alert_message,show_modal,recover_password_email,recover_password_msg};
  }
  // END MAP STATE TO PROPS
  
  
  export default connect(mapStateToProps,authAction)(Login);