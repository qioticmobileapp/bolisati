import React, { Component } from 'react';
import {ImageBackground,Dimensions,Image,TouchableOpacity} from 'react-native';
import {List,ListItem,Left,Right,Body,Icon,Text,Content, CardItem} from 'native-base';
import {transparentBackGround,transparentBorder} from '../theme';
import { signOut,isLoggIn } from "../actions/authAction";
import { connect } from 'react-redux';

import {profileNameText,homeCardItem,otherCardItemStyle,otherTextsStyle,profileImageStyle,listStyle} from '../assests/styles/sideBarStyles';
import { Actions } from 'react-native-router-flux';
const dimensions=Dimensions.get('window');
class SideBar extends Component{
  // constructor(props) {
  //   super(props);
  //   props.isLoggIn();
  // }
  //START SIGN OUT
  componentDidMount(){
    this.props.isLoggIn()
  }
  signOutHandler = () => {
    this.props.closeDrawer();
    this.props.signOut();
    Actions.firstpage()
  };
  //END SIGN OUT
    render(){

      const {user}=this.props;
      console.log("user",user)
        return(
            <ImageBackground source={require('../assests/images/splash–1.png')} style={{width:300,height:dimensions.height}}>
               {/* <Content style={{backgroundColor:"#3d335992"}}> */}
                 <List style={listStyle}>
                   <ListItem style={transparentBorder}></ListItem>
                   <CardItem style={[transparentBackGround,{borderColor:"transparent"}]}>
                     <Right>
                       <Image
                           source={require("../assests/images/Facebook-Ad-Sizes.png")}
                            style={profileImageStyle}
                        />
                     </Right>
                   </CardItem>
                   {user.data?
                   <CardItem style={[transparentBackGround,{borderColor:"transparent"}]}>
                     <Right>
                       <Text style={profileNameText}>{user.data[0].customers_firstname}</Text>
                       <Text style={profileNameText}>{user.data[0].customers_lastname}</Text>

                     </Right>
                   </CardItem>
                   :null}
                   <CardItem style={homeCardItem}>
                     <Right>
                       <Text style={otherTextsStyle}>الرئيسية</Text>
                     </Right>
                   </CardItem>
                   <CardItem style={otherCardItemStyle}>
                     <Right>
                       <Text style={otherTextsStyle}>الإشعارات</Text>
                     </Right>
                   </CardItem>
                   <CardItem style={otherCardItemStyle}>
                     <Right>
                       <Text style={otherTextsStyle}>مساعدة</Text>
                     </Right>
                   </CardItem>
                   <CardItem style={otherCardItemStyle}>
                     <Right>
                       <Text style={otherTextsStyle}>الإعدادات</Text>
                     </Right>
                   </CardItem>
                   <TouchableOpacity onPress={this.signOutHandler}>
                   <CardItem style={otherCardItemStyle}>
                  
                     <Right>
                       <Text style={otherTextsStyle}>الخروج</Text>
                     </Right>
                   </CardItem>
                   </TouchableOpacity>

                   <CardItem style={otherCardItemStyle}>
                     <Right>
                       <Text style={otherTextsStyle}>  العربية</Text>
                     </Right>
                     <Body>
                         <Text style={otherTextsStyle}>الإنجليزية</Text>
                     </Body>
                   </CardItem>

                 </List>
                {/* </Content> */}
           </ImageBackground>
        )
    }
}
const mapStateToProps = state =>
{
  const { user } = state.authReducer;
  return { user};
}
export default connect(mapStateToProps,{signOut,isLoggIn})(SideBar);