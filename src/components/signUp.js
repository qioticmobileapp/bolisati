import React,{Component} from 'react';
import  {Image,ImageBackground,Dimensions,Text,TouchableOpacity,TouchableWithoutFeedback,View,Modal} from 'react-native';
import {Body,CardItem,Button,Item,Input,Left,Right,ListItem,Content,Picker,Icon} from 'native-base';
import {Spinner} from "./common/Spinner";
import PhoneInput from 'react-native-phone-input';
import DropdownAlert from 'react-native-dropdownalert';
import * as authAction from '../actions/authAction';
import { connect } from 'react-redux';
import {Actions} from "react-native-router-flux";
import {inputNameStyle} from '../assests/styles/signUpStyles';
import {blueBackgroundColor,buttonStyle,buttonText,cardItemWithMargin,inputStyle,transparentBackground,transparentBorder,phoneInputStyle,centerStyle,touchableOpacityText,pickerStyle} from '../theme';
const dimensions=Dimensions.get('window');
class SignUp extends Component{
  constructor(props){
    super(props);
    this.state ={
      hidePassword:true,
      hideConfirmPassword:true
    }

  }
      componentDidUpdate() {
        const {signup_alert_message} = this.props;

        if (signup_alert_message != null) {
          setTimeout(() => this.props.resetAuthMessage(), 250);
        }
     

      }
    
      //START SIGN UP SUBMITTION
      handleSignUp = () => {
        

        const {first_name, last_name, password,phone_number,country,confirm_password} = this.props;
        this.props.signupUser(first_name, last_name,  password,phone_number,confirm_password);
      };
      //END SIGN UP SUBMITTION
    
      //START DROPDOWN MESSAGES
      onError = error => {
        if (error) {

          
          this.dropdown.alertWithType("error", "Error", error);
        }
      };
    
      onSuccess = success => {

        if (success) {
          this.dropdown.alertWithType("success", "Success", success);
        }
      };
      //END DROPDOWN MESSAGES
    
      //START SHOW ALERT FUNC
      showAlert = () => {
        const {signup_alert_message} = this.props;
        if (signup_alert_message != null) {
          if (signup_alert_message.isError) {
            this.onError(signup_alert_message.msg);
          } else if (signup_alert_message.isSuccess) {
            this.onSuccess(signup_alert_message.msg);
          } else {
            return;
          }
        }
      };
      //END SHOW ALERT FUNC
    
    
    
      //START RENDER SUBMIT BUTTON
      showSubmitButton() {
        const {start_loading_of}=this.props;
        if (start_loading_of.signup_loading) {
          return (
          
                <Button block style={blueBackgroundColor}>
                  <Spinner size="large" />
                </Button>
           
          );
        }

        return (
          
          <Button style={buttonStyle} block onPress={this.handleSignUp}>
          <Text style={buttonText}> إنشاء حساب</Text>
        
        </Button>
       
        
        );
      }
      //END RENDER SUBMIT BUTTON

        // START SELECT A PHONE COUNTRY
     selectCountry(country){
      const country_code  = this.phone.getCountryCode();
      this.props.getUserText({prop:'phone_number',value:{
        code:country_code,
        number:''
      }});
     }
  
  // END SELECT A PHONE COUNTRY

    // START CHANGING A PHONE NUMBER FUNC
    changePhoneNumber(number)
    {
      const { phone_number } = this.props;
     if (this.phone.getISOCode()) {
       const country_code  = this.phone.getCountryCode();
   
       this.props.getUserText({prop:'phone_number',value:{
         code:country_code,
         number:phone_number.number
       }});
     }
     else {
       this.props.getUserText({prop:'phone_number',value:{
         code:'',
         number:phone_number.number
       }});
     }
 const num = number;
     this.props.getUserText({prop:'phone_number',value:{
       code:phone_number.code,
       number:num
     }});
 
   };
    // END CHANGING A PHONE NUMBER FUNC
    managePasswordVisibility=()=>{
      this.setState({hidePassword: !this.state.hidePassword});
    }
    manageConfirmPasswordVisibility=()=>{
      this.setState({hideConfirmPassword: !this.state.hideConfirmPassword});
    }
 
    render(){
      const {password, first_name, last_name,phone_number,country,confirm_password,show_mobile_modal} = this.props;
// let initialCountry=""
//    console.log("country",country)
//    if(country==1){
//      initialCountry="jo"
//    }
//    else if (country==2){
//      initialCountry="us"
//    }
//    else if(country==3){
//      initialCountry="qa"
//    }
//    else if(country==4){
//      initialCountry=="ua"
//    }
//    else{
//      console.log("herreeee")
//      initialCountry="jo"
//    }
// console.log("initialCountry",initialCountry)

return(
  
    <ImageBackground source={require('../assests/images/splash–1.png')} style={{width:dimensions.width,height:'100%'}}>
      <Content
    ref={ref => {
      this._Content = ref;
    }}
  >
    <CardItem style={cardItemWithMargin}>
  
              <Item style={{borderColor: "transparent",display:'flex',flexDirection: 'row', justifyContent: 'space-between'}}>
             
              <Input
                  color="#000"
                  placeholder="إسم العائلة"
                  onChangeText={value =>
                    this.props.getUserText({prop: "last_name", value})
                  }
                  value={last_name}
                  style={inputNameStyle}
                />
                <Input
                  color="#000"
                  placeholder="الإسم الأول"
                  onChangeText={value =>
                    this.props.getUserText({prop: "first_name", value})
                  }
                  value={first_name}
                  style={inputStyle}
                />
         
              </Item>
             
            </CardItem>
            <CardItem style={transparentBackground}>
                  <Item style={transparentBorder}>
             
                    <Picker
                      mode="dropdown"
                      iosHeader="اختر بلد"
                      placeholder="اختر بلد"
                      iosIcon={<Icon name="arrow-down" />}
                      placeholderStyle={{ color: "#9B9B9B" }}
                      style={pickerStyle}
                      selectedValue={country}
                      onValueChange={value =>
                        this.props.getUserText({
                          prop: "country",
                          value
                        })
                      }
                     >
                     <Picker.Item label="الاردن" value="1" />
                     <Picker.Item label="السعوديه" value="2" />
                     <Picker.Item label="الامارات" value="3" />
                     <Picker.Item label="قطر" value="4" />
                    </Picker>
                </Item>
               </CardItem>

            <CardItem style={transparentBackground}>
              <Item style={transparentBorder}>
              <PhoneInput  ref={(ref) => { this.phone = ref; }}
                    initialCountry="jo"
                    style={phoneInputStyle}
                    value =""
                    flagStyle={{marginLeft:10,height:30,width:40,flex:0.2}}
                    textStyle={{fontSize:18}}
                    onSelectCountry ={(country) => this.selectCountry(country)}
                    onChangePhoneNumber= {(number)=> this.changePhoneNumber(number)}
                    isValidNumber
                    
                  /> 
        
              </Item>
            </CardItem>

          
            <CardItem style={transparentBackground}>
              <Item style={transparentBorder}> 
                <Input
                  color="#fff"
                  onChangeText={value =>
                    this.props.getUserText({prop: "password", value})
                  }
                  value ={password}
                  placeholder ="كلمة المرور"
                  secureTextEntry={this.state.hidePassword}
                  style={inputStyle}
                />
                <TouchableOpacity activeOpacity={0.8} style={{position: 'absolute',height: 40,width: 35,padding: 5}} onPress={this.managePasswordVisibility}>
                  <Image source={(this.state.hidePassword) ? require('../assests/images/hideeye.png') : require('../assests/images/eye.png')} style={{resizeMode:'contain',height: '70%',width: '70%'}} />
                </TouchableOpacity>
               
              </Item>
            </CardItem>

            <CardItem style={transparentBackground}>
              <Item style={transparentBorder}> 
                <Input
                  color="#fff"
                  onChangeText={value =>
                    this.props.getUserText({prop: "confirm_password", value})
                  }
                  value ={confirm_password}
                  placeholder ="تأكيد كلمة المرور"
                  secureTextEntry={this.state.hideConfirmPassword}
                  style={inputStyle}
                />
                                <TouchableOpacity activeOpacity={0.8} style={{position: 'absolute',height: 40,width: 35,padding: 5}} onPress={this.manageConfirmPasswordVisibility}>
                  <Image source={(this.state.hideConfirmPassword) ? require('../assests/images/hideeye.png') : require('../assests/images/eye.png')} style={{resizeMode:'contain',height: '70%',width: '70%'}} />
                </TouchableOpacity>
              </Item>
            </CardItem>

            <CardItem style={transparentBackground}>
           <View style={{marginLeft:50}}>
           <Text style={{textAlign:"right",fontFamily:'Tajawal-Medium',fontSize:10}}>بالضغط على زر انشاء حساب أوافق على اتفاقية المستخدم وسياسة الخصوصية </Text>
           </View>
           </CardItem>
 
            <CardItem style={transparentBackground}>
         <Body style={centerStyle}>
         {this.showSubmitButton()}

         </Body>
            </CardItem>
            <CardItem style={transparentBackground}>
         <Body style={centerStyle}>
         <TouchableOpacity onPress={() => Actions.login()}>
         
            <Text style={touchableOpacityText}> هل لديك حساب؟ تسجيل الدخول</Text>
         
          </TouchableOpacity> 
           </Body>
        </CardItem>
        <Modal
            visible={show_mobile_modal}
            animationType={"slide"}
            onRequestClose={() =>
              this.props.showMobileCodeModal(!show_mobile_modal)
            }
            supportedOrientations={[
              "portrait",
              "portrait-upside-down",
              "landscape",
              "landscape-left",
              "landscape-right"
            ]}
            transparent
          >
            <TouchableWithoutFeedback
              onPress={() => this.props.showMobileCodeModal(!show_mobile_modal)}
            >
              <View style={{backgroundColor:'rgba(0,0,0,0.50)',position:'relative',flex:1,justifyContent:'center'}}>
                <View style={{  borderWidth:1,borderRadius:5,borderColor:'#e3e3e3',padding:0,backgroundColor:'#F7F9F9',marginLeft:15,marginRight:15}}>
                  <ListItem style={{marginTop:10,alignSelf:"center",borderBottomWidth: 0}}>
                  <Text style={{color:"#003580"}}>Enter Your SMS One Time Password (OTP)</Text>
                  </ListItem>
                  <CardItem style={{backgroundColor:"#1e2131",alignSelf:"center"}}>
              <Input keyboardType='numeric' style={{borderBottomWidth:1,borderColor:'#fff',marginRight:10,marginLeft:10,color:'#fff',textAlign:'center',fontWeight:'bold',fontSize:22}} maxLength={1} ref={(ref)=>{this._firstVI = ref}} />
              <Input keyboardType='numeric' style={{borderBottomWidth:1,borderColor:'#fff',marginRight:10,marginLeft:10,color:'#fff',textAlign:'center',fontWeight:'bold',fontSize:22}} maxLength={1} ref={(ref)=>{this._secondVI = ref}} />
              <Input keyboardType='numeric' style={{borderBottomWidth:1,borderColor:'#fff',marginRight:10,marginLeft:10,color:'#fff',textAlign:'center',fontWeight:'bold',fontSize:22}} maxLength={1} ref={(ref)=>{this._thirdVI = ref}}  />
              <Input keyboardType='numeric' style={{borderBottomWidth:1,borderColor:'#fff',marginRight:10,marginLeft:10,color:'#fff',textAlign:'center',fontWeight:'bold',fontSize:22}} maxLength={1}  ref={(ref)=>{this._fourthVI = ref}} />
              <Input keyboardType='numeric' style={{borderBottomWidth:1,borderColor:'#fff',marginRight:10,marginLeft:10,color:'#fff',textAlign:'center',fontWeight:'bold',fontSize:22}} maxLength={1}  ref={(ref)=>{this._fifthVI = ref}} />
              <Input keyboardType='numeric' style={{borderBottomWidth:1,borderColor:'#fff',marginRight:10,marginLeft:10,color:'#fff',textAlign:'center',fontWeight:'bold',fontSize:22}} maxLength={1}  ref={(ref)=>{this._sixthVI = ref}} />
            </CardItem>

                  
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
          </Content>
        <Text>{this.showAlert()}</Text>

        <DropdownAlert ref={ref => (this.dropdown = ref)} />

    </ImageBackground>

)
    }
}
//START MAP STATE TO PROPS
const mapStateToProps = state => {
  const {
 

    password,
    start_loading_of,
    first_name,
    last_name,
    phone_number,
    confirm_password,
    country,
    signup_alert_message,
    show_mobile_modal  } = state.authReducer;
  return {

    password,
    start_loading_of,
    first_name,
    last_name,
    phone_number,
    confirm_password,
    country,
    signup_alert_message,
    show_mobile_modal,
  };
};
//END MAP STATE TO PROPS

export default connect(mapStateToProps, authAction)(SignUp);