import {SIGININ_MSG,
      SIGINUP_MSG,
      GET_USER_TEXT,
      START_AUTH_LOADING,
      SHOW_RECOVER_PASSWORD_MODAL,
      RECOVER_PASS_MSG,
      CET_RECOVER_PASSWORD_EMAIL_TEXT,
      IS_USER_LOGGEDIN,
      SHOW_MOBILE_CODE_MODAL,
      RESET_MSG
      
}from '../actions/types';

  const initialState ={
    password:'',
    confirm_password:'',
    email:'',
    first_name:'',
    last_name:'',
    phone_number:{
        code:1,
        number:''
        },
        user: {
            isLoggedIn: false,
            data: null
          },
    country:'',
    signin_alert_message:null,
    signup_alert_message: null,
    show_modal:false,
    show_mobile_modal:false,
    recover_email:'',
    recover_password_msg:null,
    recover_password_email:'',
    start_loading_of:{
        signin_loading:false,
        signup_loading:false,
        recover_loading:false
      }  };

  export default (state = initialState, action) => {
    switch (action.type) {
        case SIGININ_MSG:
        return {...state,signin_alert_message:action.payload,recover_email:''};

        case GET_USER_TEXT:
        return {...state,[action.payload.prop]:action.payload.value};

        case START_AUTH_LOADING:
        return {...state,start_loading_of:action.payload};

        case SHOW_RECOVER_PASSWORD_MODAL:
        return {...state, show_modal: action.payload};

        case SHOW_MOBILE_CODE_MODAL:
        return {...state, show_mobile_modal: action.payload};
        
        case IS_USER_LOGGEDIN:
        return {...state, user: action.payload};

        case RECOVER_PASS_MSG:
        return {...state,recover_password_msg:action.payload}

        case CET_RECOVER_PASSWORD_EMAIL_TEXT:
        return {...state,[action.payload.prop]:action.payload.value};

        case SIGINUP_MSG:
        return {
          ...state,
          signup_alert_message: action.payload,
          recover_email: ""
        };
        case RESET_MSG:
        return {
          ...state,
          signin_alert_message: null,
          signup_alert_message: null,
          recover_password_msg: ""
        };

        default:
            return state;
    }
}
