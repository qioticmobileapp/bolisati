import {
    GET_CATEGORIES,
    HOME_LOADING
  
    } from '../actions/types';
  
  const INITIAL_STATE = {
    categories: [],
    home_loading:false
  }
  
  export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case GET_CATEGORIES:
        return {
          ...state,
          categories: action.payload
        }

        case HOME_LOADING:
        return {...state,home_loading:action.payload };
     
  
      default:
        return state;
  
    }
  }
  