import {combineReducers} from 'redux';
import sampleReducer from './sampleReducer'
import authReducer from './authReducer'
import homeReducer from './homeReducer'
import MobileAuthenticationReducer from './mobileAuthenticationReducer'
import CarInformationReducer from './carInformationReducer'
import InsuranceCompaniesReducer from './insuranceCompaniesReducer';
export default combineReducers({
    sampleReducer : sampleReducer,
    authReducer : authReducer,
    homeReducer:homeReducer,
    phone_auth_state:MobileAuthenticationReducer,
    carInformarionReducer:CarInformationReducer,
    insuranceCompaniesReducer:InsuranceCompaniesReducer
})