import {SHOW_INSURANCE_COMPANY_MODAL,GET_ADDONS,GET_TERMS} from '../actions/types';
  
  const INITIAL_STATE = {
      show_insurance_modal:false ,
      addons:[],
      terms:[]
     }
  
  export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SHOW_INSURANCE_COMPANY_MODAL:
        return {...state, show_insurance_modal: action.payload};

        case GET_ADDONS:
        return {...state,addons: action.payload
        }

        case GET_TERMS:
        return {...state,terms: action.payload
        }
  
      default:
        return state;
  
    }
  }
  