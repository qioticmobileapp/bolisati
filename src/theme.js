import { StyleSheet,Dimensions } from 'react-native';
const dimensions=Dimensions.get('window');

module.exports  = StyleSheet.create({
 centerStyle:{
    justifyContent:'center',alignItems:'center'
 },
 transparentBackGround:{
    backgroundColor:"transparent",
    borderWidth:1,
    borderRadius:5
 },
 blueBackgroundColor:{
   backgroundColor: "#003580"
 },
 buttonStyle:{
   backgroundColor:'#003580',justifyContent:'center',marginTop:10
},

buttonText:{
   color:"#fcfcfc",fontSize:16,fontFamily:'Tajawal-Bold'
},
cardItemWithMargin:{
   backgroundColor:"transparent",marginTop:50
},
inputStyle:{
   borderRadius:5,marginBottom:5,backgroundColor:"#fff",textAlign:"right",height:45
},
transparentBackground:{
   backgroundColor:"transparent"
},
transparentBorder:{
borderColor:"transparent"
},
phoneInputStyle:{
   borderRadius:5,marginBottom:5,backgroundColor:"#fff",textAlign:"right",width:340,height:45
},
touchableOpacityText:{
   color:"#171717",fontSize:10,fontFamily:'Tajawal-Bold'
},
servicesText:{
   color:"#003580",
   fontSize:21,
   fontFamily:'Tajawal-Black'
},
sevicesCardItemStyle:{
   backgroundColor:"#fff",
   height:66,
   marginTop:15
},
pickerStyle:{
   borderRadius:5,marginBottom:5,backgroundColor:"#fff",width:340,height:45,direction: 'rtl'
},
datePickerStyle:{
   width:'100%',backgroundColor:"#fff",borderColor:"#fff"
},
headerStyle:{
   backgroundColor:"#003580",height:108.3,width:dimensions.width
},
menuIcon:{
   color:'#FFF',fontSize:35,fontWeight:'bold'
}
  })